FROM golang:1.21.1-alpine 

WORKDIR /app

#copia o arquivo das dependencias do projeto para a pasta criada em workdir
COPY go.mod go.sum ./

#baixa as dependencias solicitadas acima
RUN go mod download 

#copia todos os arquivos em .go para dentro de workdir
COPY *.go ./

#compila os comandos para criar em um arquivo executavel
RUN go build -o /go-app

#abre a porta no container
EXPOSE 3000

CMD [ "/go-app" ]